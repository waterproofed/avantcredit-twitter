
# twitter

this is the solution to the twitter programming challenge

## installation

```bash

    git clone git@bitbucket.org:waterproofed/avantcredit-twitter.git

    cd avantcredit-twitter

    virtualenv -p /opt/bin/python2.7 venv

    source venv/bin/activate

    pip install -r requirements.txt

```

## running

```bash

    ./bin/wc --consumer-key='aaaa' --consumer-secret='bbbb' --access-token='cccc' --access-token-secret='dddd'

```

there are additional command line flags:

* --seconds=int
  the number of seconds to sample

* --words=int
  the top number words by frequency to output

## tests

```bash

    nosetests

```

## notes

this was developed under python 2.7 (compiled from source) under OSX

## remarks

if the program had to restart i would read/write the state of the counter to a file when the program exited,
the name of the file being a command line parameter.

