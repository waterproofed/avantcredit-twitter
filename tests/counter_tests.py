from challenge import words

from nose import tools

def test_increment_and_top():
    counter = words.Counter(words.STOP_WORDS)
    for i in range(10):
        counter.increment('hello')
    for i in range(5):
        counter.increment('world')
    actual = counter.top(1)
    tools.assert_equal(len(actual), 1)
    tools.assert_equal(actual[0], 'hello')

