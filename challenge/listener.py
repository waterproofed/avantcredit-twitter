from tweepy.streaming import StreamListener
from datetime import datetime
from json import loads

class CountingListener(StreamListener):

    def __init__(self, counter, seconds):
        self.counter = counter
        self.start   = datetime.now()
        self.seconds = seconds

    def on_data(self, raw_data):
        d0 = self.start
        d1 = datetime.now()
        if (d1-d0).total_seconds() > self.seconds:
            return False
        data=loads(raw_data)
        if 'text' in data:
            for word in data['text'].split():
                self.counter.increment(word)
        return True

